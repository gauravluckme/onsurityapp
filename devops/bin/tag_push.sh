#!/usr/bin/bash -eu

docker tag 3tier_api:latest ${REGISTRY}/three_tier_api
docker push ${REGISTRY}/three_tier_api

docker tag 3tier_web:latest ${REGISTRY}/three_tier_web
docker push ${REGISTRY}/three_tier_web
