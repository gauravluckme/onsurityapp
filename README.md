# 3Tier application for Onsurity DevOps Assignment

This is the NodeJs 3Tier application.

# Requirements

We are using CI tool as Bitbucket Cloud Pipelines, so will be using docker image for steps (or stages)
and will install docker-compose using depencies.sh script for each step ( or stages)

To setup AWS configure in Bitbucket, below steps needs to be followed:-

1. Open your Repository settings > Repository Variables
2. Create AWS_ACCESS_KEY_ID with your AWS Api Key (mark as secret)
3. Create AWS_SECRET_ACCESS_KEY with your AWS Secret Api Key (mark as secret)
4. Create AWS_DEFAULT_REGION with an AWS Region (optional if not set in your AWS provider file)
5. The variable for ECR registry also need to set. Ex. REGISTRY="${ACCOUNT_ID}.dkr.ecr.us-east-1.amazonaws.com"

Or if we are using some other CI tools the below application need to be installed.

* [Docker](https://www.docker.com/get-started)
* [Docker-Compose](https://docs.docker.com/compose/install/)
  * This comes bundled with Docker on Windows.
* [Terraform](https://www.terraform.io/downloads.html)
    * Manages IaaS resources, such as AWS servers
    * Make sure `terraform` is in your PATH.

Everything else runs within the Docker containers setup by the
[docker-compose.yml](docker-compose.yaml) file.

# Deployment

All commands are described assuming you are in a Bash shell on your host
machine in the root directory of this repository.

There are a number of one-time actions that must take place before the Bitbucket CI/CD
pipeline can run code updates.

* ./devops/bin/bitbucket.sh first push the code to the AWS ECR repository and then deploy the application using AWS ECS service

## One-time actions

### Required AWS privileges

Ensure you have set your AWS credentials correctly for an account with
(at least) the following authorizations:
  * CloudWatchFullAccess
  * DynamoDBFullAccess
  * ECSFullAccess
  * IAMFullAccess
  * S3FullAccess
  * RDSFullAccess
  * VPCFullAccess

The AWS credentials need to added in --> Repository Settings -> Repository Variables in Bibucket.

### Setup Terraform minimums

In order for Terraform to be safely run in a non-single-user environment, the
tfstate files must be stored in a central place and a locking mechanism must be
enabled. For this project, we will the [S3 backend](https://www.terraform.io/docs/backends/types/s3.html).

Do the following work:

* In `devops/infrastructure/terraform/devops/main.tf`, comment out the section
marked.
* `(cd devops/infrastructure/terraform/devops; terraform init)`
* `(cd devops/infrastructure/terraform/devops; terraform apply -var-file prod.tfvars -auto-approve)`
* In `devops/infrastructure/terraform/devops/main.tf`, uncomment the section.
* `(cd devops/infrastructure/terraform/devops; terraform init)`
  * You will be asked to transfer the state. Say yes.
* `(cd devops/infrastructure/terraform/devops; terraform apply -var-file prod.tfvars -auto-approve)`
  * This should result in "0 added, 0 changed, 0 destroyed"

### Build the permanent infrastructure

* `(cd devops/infrastructure/terraform/3tier; terraform init)`
* The first time, comment out the cluster definitions. There is a comment in each cluster defition TF file for what and why.
* `(cd devops/infrastructure/terraform/3tier; terraform apply -var-file prod.tfvars -auto-approve)`
  * This will take a while the first time it is run.
* `./devops/bin/tag_push.sh`
  * This assumes you have run `docker-compose up -build` at least once.
* Uncomment what you commented.
* `(cd devops/infrastructure/terraform/3tier; terraform apply -var-file prod.tfvars -auto-approve)`
  * Even though it will return quickly, the tasks will take a while to start.

### Deploying a new version of the app

* `./devops/bin/ecr_login.sh`
* `./devops/bin/tag_push.sh`

## Deployment

### Multiple Terraforms

All of the Terraform for this project are in one bunch. This is fragile,
both as the number of elements in the project grows and as the number of
projects overall grows. The appropriate solution is to put things that should
be destroyed together in one bunch and capture appropriate outputs to a JSON
file stored in an S3 bucket. Then, to use these outputs as inputs to another
chunk of Terraform, wrapper scripting needs to be created around Terraform to
read the appropriate JSON file(s) from the S3 bucket and inject the required
variables. For example, `vpc_id`, `subnet_id`, and similar values should be
created in one Terraform group, then reused in other groups. 